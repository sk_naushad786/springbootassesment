package com.telstra.codechallenge.service.impl;

import java.time.LocalDate;

import org.springframework.stereotype.Service;

import com.telstra.codechallenge.domain.GitRepositoryWrapper;
import com.telstra.codechallenge.service.GitApiService;
import com.telstra.codechallenge.service.GitRepositoryService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class GitRepositoryServiceImpl implements GitRepositoryService {
	private final GitApiService gitApiService;

	@Override
	public GitRepositoryWrapper findHottestGitRepo(Integer size) {
		String q = "created:" + getLastWeekDateRange();
		String sortingField = "stars";
		String sortingOrder = "desc";

		return gitApiService.searchRepository(q, sortingField, sortingOrder, size);
	}

	private String getLastWeekDateRange() {
		LocalDate now = LocalDate.now();
		LocalDate weekStartDate = now.minusDays(7 + now.getDayOfWeek().getValue() - 1);
		LocalDate weekEndDate = now.minusDays(now.getDayOfWeek().getValue());

		return weekStartDate + ".." + weekEndDate;
	}
}
