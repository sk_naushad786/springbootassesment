package com.telstra.codechallenge.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.telstra.codechallenge.domain.GitRepositoryWrapper;

import feign.Headers;

@FeignClient(value = "git-api", url = "https://api.github.com/search/repositories")
public interface GitApiService {

	@Headers("accept: application/vnd.github.preview")
	@GetMapping("?q=created:2020-05-18..2020-05-24&sort=stars&order=desc")
	GitRepositoryWrapper searchRepository(@RequestParam("q") String q, @RequestParam("sort") String sortingField,
			@RequestParam("order") String sortingOrder, @RequestParam("per_page") Integer size);
}
