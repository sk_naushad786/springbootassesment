package com.telstra.codechallenge.service;

import com.telstra.codechallenge.domain.GitRepositoryWrapper;

public interface GitRepositoryService {
	GitRepositoryWrapper findHottestGitRepo(Integer size);
}
