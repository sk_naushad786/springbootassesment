package com.telstra.codechallenge.domain;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(value = Include.NON_NULL)
@Setter
@Getter
public class GitRepositoryWrapper {
	
	@JsonProperty("total_count")
	private Long totalCount;
	
	Set<GitRepository> items;
}
