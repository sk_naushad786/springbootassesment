package com.telstra.codechallenge.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GitRepository {
	private String html_url;
	private Integer watchers_count;
	private String language;
	private String description;
	private String name;
}
