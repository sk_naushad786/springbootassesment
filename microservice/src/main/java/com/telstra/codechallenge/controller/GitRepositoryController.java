package com.telstra.codechallenge.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.codechallenge.domain.GitRepositoryWrapper;
import com.telstra.codechallenge.service.GitRepositoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/repositories")
@Api(description = "Git repositories supported apis")
public class GitRepositoryController {
	private final Logger logger = LoggerFactory.getLogger(GitRepositoryController.class);
	private final GitRepositoryService gitRepositoryService;
	
	
	@GetMapping
	@ApiOperation("Find previous week hottest repositories")
	public ResponseEntity<GitRepositoryWrapper> findHottestGitRepo(@RequestParam(name = "size", required = false) Integer size) {
		logger.info("Rest request to find hottest git repository of previous week");
		
		if (Optional.ofNullable(size).isPresent() && size == 0)
			return ResponseEntity.ok().build();

		return ResponseEntity.ok(gitRepositoryService.findHottestGitRepo(size));
	}
}
