Feature: As a developer i want to test find hottest repository api

  Scenario: Is findGitRepository endpoint uri available
    Given url microserviceUrl
    And path '/api/v1/repositories'
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    
  Scenario: "Does findGitRepository accept request parameter 'size' to limit endpoint's response count"
    Given url microserviceUrl
    And path '/api/v1/repositories'
    And params {"size":5}
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    * def gitRepoSchema = { html_url : '#string', name: '#string', description: '#string', language: '#ignore', watchers_count : '#number'}
    And match response.items == '#[5] gitRepoSchema'
  
  Scenario: "Validate findGitRepository endpoint's response without size parameter"
    Given url microserviceUrl
    And path '/api/v1/repositories'
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    * def gitRepoSchema = { html_url : '#string', name: '#string', description: '#string', language: '#ignore', watchers_count : '#number'}
    And assert response.items.length > 0
    
    
